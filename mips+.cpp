//
//  main.cpp
//  Misp
//
//  Created by 李江贝 on 2017/6/26.
//  Copyright © 2017年 李江贝. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <stdlib.h>
using namespace std;

vector <string> dda;//读入文件

map <string, int> dataMap;//dataLabel
map <string, int> textMap;//textLabel

int pc,pile;
unsigned char data[4000000];

struct Register{
    int num;
    string address;
    bool type;//0 number; 1 address
    Register(){};
};

int rgt[33], lo, hi;
int start = 1;
bool stop = 0;
bool hazard = 0;

struct IFtoID{//一二级寄存器
    int address;
    int operno;//操作种类
    string ansno;//结果字符串
    string ques1no;//操作数1
    string ques2no;//操作数2
    IFtoID(){};
    IFtoID(int x, string y, string m, string n){
        operno = x;
        ansno = y;
        ques1no = m;
        ques2no = n;
    }
};

struct IDtoEX{//二三级寄存器
    int address;
    int operno;//操作种类
    int ansno;//储存结果寄存器
    int ques1no;//操作数1的值
    int ques2no;//操作数2的值
    IDtoEX (){};
};

struct EXtoMA{
    int address;
    int operno;
    int result;
    int _result;
    int ansno;
};

struct MAtoWB{
    int address;
    int operno;
    int result;
    int _result;
    int ansno;
};

IFtoID iti;
IDtoEX ite;
EXtoMA etm;
MAtoWB mtw;


class IF{
public:
    void run(){
        iti.address = pc;
        string s = dda[pc++];
        string s1,s2,s3;
        int i = 1;
        while(s[i] != ' ' && i < s.size()) ++i;//跳过操作种类
        ++i;
        for(int j = 1; i < s.size(); ++i) {//读取三个操作数
            if(s[i] == ' ')continue;
            if(s[i] == ','){
                ++j;
                continue;
            }
            else{
                if(j == 1){
                    s1 += s[i];
                }
                else if(j == 2){
                    s2 += s[i];
                }
                else{
                    s3 += s[i];
                }
            }
        }
        if(s[0] == 'a'){
            if(s[3] == ' '){
                iti.operno = 1;//add
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
            else if(s[4] == 'u'){//addu
                iti.operno = 2;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
            else{//addiu
                iti.operno = 3;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
        }
        else if(s[0] == 's'){
            if(s[1] == 'u'){
                if(s[3] == 'u'){//subu
                    iti.operno = 5;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
                else{//sub
                    iti.operno = 4;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
            }
            else if(s[1] == 'e'){//seq
                iti.operno = 21;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
            else if(s[1] == 'g'){
                if(s[2] == 'e'){//sge
                    iti.operno = 22;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
                else{//sgt
                    iti.operno = 23;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
            }
            else if(s[1] == 'l'){
                if(s[2] == 'e'){//sle
                    iti.operno = 24;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
                else{//slt
                    iti.operno = 25;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
            }
            else if(s[1] == 'b') {//sb
                iti.operno = 48;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else if(s[1] == 'h') {//sh
                iti.operno = 49;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else if(s[1] == 'w') {//sw
                iti.operno = 50;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else if(s[1] == 'y'){//syscall
                iti.operno = 55;
            }
            else{//sne
                iti.operno = 26;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
        }
        else if(s[0] == 'm'){
            if(s[1] == 'u'){
                if(s[3] == 'u'){
                    if(s3 == ""){//mulu
                        iti.operno = 9;
                        iti.ques1no = s1;
                        iti.ques2no = s2;
                    }
                    else{//mulu
                        iti.operno = 7;
                        iti.ansno = s1;
                        iti.ques1no = s2;
                        iti.ques2no = s3;
                    }
                }
                else {//mul
                    if(s3 == ""){
                        iti.operno = 8;
                        iti.ques1no = s1;
                        iti.ques2no = s2;
                    }
                    else{//mul
                        iti.operno = 6;
                        iti.ansno = s1;
                        iti.ques1no = s2;
                        iti.ques2no = s3;
                    }
                }
            }
            else if(s[1] == 'o'){//move
                iti.operno = 51;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else if(s[2] == 'h'){//mfhi
                iti.operno = 52;
                iti.ansno = s1;
            }
            else {//mflo
                iti.operno = 53;
                iti.ansno = s1;
            }
        }
        else if(s[0] == 'd'){
            if(s[3] == 'u'){
                if(s3 == ""){//divu
                    iti.operno = 13;
                    iti.ques1no = s1;
                    iti.ques2no = s2;
                }
                else{//divu
                    iti.operno = 11;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
            }
            else {//div
                if(s3 == ""){
                    iti.operno = 12;
                    iti.ques1no = s1;
                    iti.ques2no = s2;
                }
                else{
                    iti.operno = 10;
                    iti.ansno = s1;
                    iti.ques1no = s2;
                    iti.ques2no = s3;
                }
            }
        }
        else if(s[0] == 'x') {
            if(s[3] == 'u') {//xoru
                iti.operno = 15;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
            else {//xor
                iti.operno = 14;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
        }
        else if(s[0] == 'n'){
            if(s[1] == 'o'){//nop
                iti.operno = 54;
            }
            else if(s[3] == 'u') {//negu
                iti.operno = 17;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else {//neg
                iti.operno = 16;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
        }
        else if(s[0] == 'r'){
            if(s[3] == 'u') {//remu
                iti.operno = 19;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
            else {//rem
                iti.operno = 18;
                iti.ansno = s1;
                iti.ques1no = s2;
                iti.ques2no = s3;
            }
        }
        else if(s[0] == 'l'){
            if(s[1] == 'i') {//li
                iti.operno = 20;
                iti.ansno = s1;
                iti.ques2no = s2;
            }
            else if(s[1] == 'a'){//la
                iti.operno = 44;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else if(s[1] == 'b'){//lb
                iti.operno = 45;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else if(s[1] == 'h'){//lh
                iti.operno = 46;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
            else {//lw
                iti.operno = 47;
                iti.ansno = s1;
                iti.ques1no = s2;
            }
        }
        else if(s[0] == 'b') {
            if(s[1] == ' '){//b
                iti.operno = 27;
                iti.ansno = s1;
            }
            else if(s[3] == 'z') {
                if(s[1] == 'e') {//beqz
                    iti.operno = 34;
                    iti.ansno = s2;
                    iti.ques1no = s1;
                }
                else if(s[1] == 'n') {//bnez
                    iti.operno = 35;
                    iti.ansno = s2;
                    iti.ques1no = s1;
                }
                else if(s[1] == 'l'){
                    if(s[2] == 'e') {//blez
                        iti.operno = 36;
                        iti.ansno = s2;
                        iti.ques1no = s1;
                    }
                    else {//bltz
                        iti.operno = 39;
                        iti.ansno = s2;
                        iti.ques1no = s1;
                    }
                }
                else{
                    if(s[2] == 'e') {//bgez
                        iti.operno = 37;
                        iti.ansno = s2;
                        iti.ques1no = s1;
                    }
                    else {//bgtz
                        iti.operno = 38;
                        iti.ansno = s2;
                        iti.ques1no = s1;
                    }
                }
            }
            else {
                if(s[1] == 'e') {//beq
                    iti.operno = 28;
                    iti.ansno = s3;
                    iti.ques1no = s1;
                    iti.ques2no = s2;
                }
                else if(s[1] == 'n') {//bne
                    iti.operno = 29;
                    iti.ansno = s3;
                    iti.ques1no = s1;
                    iti.ques2no = s2;
                }
                else if(s[1] == 'l'){
                    if(s[2] == 'e') {//ble
                        iti.operno = 31;
                        iti.ansno = s3;
                        iti.ques1no = s1;
                        iti.ques2no = s2;
                    }
                    else {//blt
                        iti.operno = 33;
                        iti.ansno = s3;
                        iti.ques1no = s1;
                        iti.ques2no = s2;
                    }
                }
                else{
                    if(s[2] == 'e') {//bge
                        iti.operno = 30;
                        iti.ansno = s3;
                        iti.ques1no = s1;
                        iti.ques2no = s2;
                    }
                    else {//bgt
                        iti.operno = 32;
                        iti.ansno = s3;
                        iti.ques1no = s1;
                        iti.ques2no = s2;
                    }
                }
            }
        }
        else {
            if(s[1] == ' ') {//j
                iti.operno = 40;
                iti.ansno = s1;
            }
            else if(s[1] == 'r') {//jr
                iti.operno = 41;
                iti.ansno = s1;
            }
            else if(s[3] == 'r') {//jalr
                iti.operno = 43;
                iti.ansno = s1;
            }
            else {//jal
                iti.operno = 42;
                iti.ansno = s1;
            }
        }
        
    }
};

class ID{
public:
    void run() {
        if(iti.operno == 55 && (ite.ansno != -1 || etm.ansno != -1 || mtw.ansno != -1)){
            hazard = 1;
            return;
        }
        else if(iti.operno == 55) {
            ite.operno = iti.operno;
            ite.ques1no = rgt[2];
            ite.ques2no = rgt[4];
            ite.ansno = 2;
            return;
        }
        ite.operno = iti.operno;//结果
        ite.address = iti.address;
        string s = iti.ansno;
        int i = 0;
        if(s[i] != '$'){//goto label
            ite.ansno = textMap[s];
        }//选择寄存器
        else if(s[i + 1] == 'z'){
            ite.ansno = 0;
        }
        else if(s[i + 1] == 'a'){
            if(s[i + 2] == '0'){
                ite.ansno = 4;
            }
            else if(s[i + 2] == '1') {
                ite.ansno = 5;
            }
            else if(s[i + 2] == '2'){
                ite.ansno = 6;
            }
            else if(s[i + 2] == '3') {
                ite.ansno = 7;
            }
            else {
                ite.ansno = 1;
            }
        }
        else if(s[i + 1] == 'v') {
            if(s[i + 2] == '0') {
                ite.ansno = 2;
            }
            else {
                ite.ansno = 3;
            }
        }
        else if(s[i + 1] == 't'){
            if(s[i + 2] == '0') {
                ite.ansno = 8;
            }
            else if(s[i + 2] == '1') {
                ite.ansno = 9;
            }
            else if(s[i + 2] == '2') {
                ite.ansno = 10;
            }
            else if(s[i + 2] == '3') {
                ite.ansno = 11;
            }
            else if(s[i + 2] == '4') {
                ite.ansno = 12;
            }
            else if(s[i + 2] == '5') {
                ite.ansno = 13;
            }
            else if(s[i + 2] == '6') {
                ite.ansno = 14;
            }
            else if(s[i + 2] == '7') {
                ite.ansno = 15;
            }
            else if(s[i + 2] == '8') {
                ite.ansno = 24;
            }
            else{
                ite.ansno = 25;
            }
        }
        else if(s[i + 1] == 's') {
            if(s[i + 2] == '0') {
                ite.ansno = 16;
            }
            else if(s[i + 2] == '1') {
                ite.ansno = 17;
            }
            
            else if(s[i + 2] == '2') {
                ite.ansno = 18;
            }
            else if(s[i + 2] == '3') {
                ite.ansno = 19;
            }
            else if(s[i + 2] == '4') {
                ite.ansno =  20;
            }
            else if(s[i + 2] == '5') {
                ite.ansno = 21;
            }
            else if(s[i + 2] == '6') {
                ite.ansno = 22;
            }
            else if(s[i + 2] == '7') {
                ite.ansno = 23;
            }
            else if(s[i + 2] == 'p') {
                ite.ansno = 29;
            }
            else {
                ite.ansno = 30;
            }
        }
        else if(s[i + 1] == 'k') {
            if(s[i + 2] == '0'){
                ite.ansno = 26;
            }
            else{
                ite.ansno = 27;
            }
        }
        else if(s[i + 1] == 'g') {
            ite.ansno = 28;
        }
        else if(s[i + 1] == 'r'){
            ite.ansno = 31;
        }
        else if(s[i + 1] == 'f') {
            ite.ansno = 30;
        }
        else {
            ite.ansno = 0;
            ++i;
            while(i < s.size()){
                ite.ansno = ite.ansno * 10 + (s[i] - '0');
                ++i;
            }
        }//变量1
        int tmp = 0;
        i = 0;
        int num = 0;
        s = iti.ques1no;
        bool judge = 0;
        bool type = 0;
        if(s[0] == '-') {
            judge = 1;
            ++i;
        }
        else {
            judge = 0;
        }
        if(s[i] != '$'){
            if(s[i] >= '0' && s[i] <= '9'){
                while(s[i] != '('){
                    tmp = tmp * 10 + (s[i] - '0');
                    ++i;
                }
                if(judge == 1) {
                    tmp = -tmp;
                }
                ++i;
            }
            else {
                type = 1;
            }
        }
        if(type == 1){
            num = dataMap[s];
        }
        else if(s[i + 1] == 'z'){
            num = 0;
        }
        else if(s[i + 1] == 'a'){
            if(s[i + 2] == '0'){
                num = 4;
            }
            else if(s[i + 2] == '1') {
                num = 5;
            }
            else if(s[i + 2] == '2'){
                num = 6;
            }
            else if(s[i + 2] == '3') {
                num = 7;
            }
            else {
                num = 1;
            }
        }
        else if(s[i + 1] == 'v') {
            if(s[i + 2] == '0') {
                num = 2;
            }
            else {
                num = 3;
            }
        }
        else if(s[i + 1] == 't'){
            if(s[i + 2] == '0') {
                num = 8;
            }
            else if(s[i + 2] == '1') {
                num = 9;
            }
            else if(s[i + 2] == '2') {
                num = 10;
            }
            else if(s[i + 2] == '3') {
                num = 11;
            }
            else if(s[i + 2] == '4') {
                num = 12;
            }
            else if(s[i + 2] == '5') {
                num = 13;
            }
            else if(s[i + 2] == '6') {
                num = 14;
            }
            else if(s[i + 2] == '7') {
                num = 15;
            }
            else if(s[i + 2] == '8') {
                num = 24;
            }
            else{
                num = 25;
            }
        }
        else if(s[i + 1] == 's') {
            if(s[i + 2] == '0') {
                num = 16;
            }
            else if(s[i + 2] == '1') {
                num = 17;
            }
            
            else if(s[i + 2] == '2') {
                num = 18;
            }
            else if(s[i + 2] == '3') {
                num = 19;
            }
            else if(s[i + 2] == '4') {
                num =  20;
            }
            else if(s[i + 2] == '5') {
                num = 21;
            }
            else if(s[i + 2] == '6') {
                num = 22;
            }
            else if(s[i + 2] == '7') {
                num = 23;
            }
            else if(s[i + 2] == 'p') {
                num = 29;
            }
            else {
                num = 30;
            }
        }
        else if(s[i + 1] == 'k') {
            if(s[i + 2] == '0'){
                num = 26;
            }
            else{
                num = 27;
            }
        }
        else if(s[i + 1] == 'g') {
            num = 28;
        }
        else if(s[i + 1] == 'r'){
            num = 31;
        }
        else if(s[i + 1] == 'f') {
            num = 30;
        }
        else {
            num = 0;
            ++i;
            while(s[i] != ')' && i < s.size()){
                num = num * 10 + (s[i] - '0');
                ++i;
            }
        }
        if(type == 1){
            ite.ques1no = num;
        }
        else{
            ite.ques1no = rgt[num] + tmp;
            if((num == mtw.ansno || etm.ansno == num)){
                hazard = 1;
                //cout<<mtw.address<<"###"<<ite.address<<endl;
                return;
            }
        }
        s = iti.ques2no;//操作数2
        tmp = 0;
        if(s[0] == '-'){
            i = 1;
            while(i < s.size()){
                tmp = tmp * 10 + (s[i] - '0');
                ++i;
            }
            ite.ques2no = -tmp;
        }
        else if(s[0] >= '0' && s[0] <= '9'){
            i = 0;
            while(i < s.size()) {
                tmp = tmp * 10 + (s[i] - '0');
                ++i;
            }
            ite.ques2no = tmp;
        }
        else {
            i = 0;
            if(s[i + 1] == 'z'){
                num = 0;
            }
            else if(s[i + 1] == 'a'){
                if(s[i + 2] == '0'){
                    num = 4;
                }
                else if(s[i + 2] == '1') {
                    num = 5;
                }
                else if(s[i + 2] == '2'){
                    num = 6;
                }
                else if(s[i + 2] == '3') {
                    num = 7;
                }
                else {
                    num = 1;
                }
            }
            else if(s[i + 1] == 'v') {
                if(s[i + 2] == '0') {
                    num = 2;
                }
                else {
                    num = 3;
                }
            }
            else if(s[i + 1] == 't'){
                if(s[i + 2] == '0') {
                    num = 8;
                }
                else if(s[i + 2] == '1') {
                    num = 9;
                }
                else if(s[i + 2] == '2') {
                    num = 10;
                }
                else if(s[i + 2] == '3') {
                    num = 11;
                }
                else if(s[i + 2] == '4') {
                    num = 12;
                }
                else if(s[i + 2] == '5') {
                    num = 13;
                }
                else if(s[i + 2] == '6') {
                    num = 14;
                }
                else if(s[i + 2] == '7') {
                    num = 15;
                }
                else if(s[i + 2] == '8') {
                    num = 24;
                }
                else{
                    num = 25;
                }
            }
            else if(s[i + 1] == 's') {
                if(s[i + 2] == '0') {
                    num = 16;
                }
                else if(s[i + 2] == '1') {
                    num = 17;
                }
                
                else if(s[i + 2] == '2') {
                    num = 18;
                }
                else if(s[i + 2] == '3') {
                    num = 19;
                }
                else if(s[i + 2] == '4') {
                    num =  20;
                }
                else if(s[i + 2] == '5') {
                    num = 21;
                }
                else if(s[i + 2] == '6') {
                    num = 22;
                }
                else if(s[i + 2] == '7') {
                    num = 23;
                }
                else if(s[i + 2] == 'p') {
                    num = 29;
                }
                else {
                    num = 30;
                }
            }
            else if(s[i + 1] == 'k') {
                if(s[i + 2] == '0'){
                    num = 26;
                }
                else{
                    num = 27;
                }
            }
            else if(s[i + 1] == 'g') {
                num = 28;
            }
            else if(s[i + 1] == 'r'){
                num = 31;
            }
            else if(s[i + 1] == 'f') {
                num = 30;
            }
            else {
                num = 0;
                ++i;
                while(i < s.size()){
                    num = num * 10 + (s[i] - '0');
                    ++i;
                }
            }
            ite.ques2no = rgt[num];
            if((num == mtw.ansno || num == etm.ansno)) {
                hazard = 1;
                //cout<<mtw.address<<"###"<<ite.address<<endl;
                return ;
            }
        }
    }
};


class EX{
public:
    void run(){
        etm.address = ite.address;
        etm.operno = ite.operno;
        etm.ansno = ite.ansno;
        long long int x;
        switch (ite.operno){
            case 1:
            case 2:
            case 3://加
                etm.result = ite.ques1no + ite.ques2no;
                break;
            case 4:
            case 5://减
                etm.result = ite.ques1no - ite.ques2no;
                break;
            case 6:
            case 7:
                etm.result = ite.ques1no * ite.ques2no;
                break;
            case 8:
            case 9:
                x = ite.ques1no * ite.ques2no;
                etm.result = x % ((long long )1 << 32);
                etm._result = x >> 32;
                break;
            case 10:
            case 11:
                etm.result = ite.ques1no / ite.ques2no;
                break;
            case 12:
            case 13:
                etm.result = ite.ques1no / ite.ques2no;
                etm._result = ite.ques1no % ite.ques2no;
                break;
            case 14:
            case 15:
                etm.result = ite.ques1no ^ ite.ques2no;
                break;
            case 16:
            case 17:
                etm.result = -ite.ques1no;
                break;
            case 18:
            case 19:
                etm.result = ite.ques1no % ite.ques2no;
                break;
            case 20:
                etm.result = ite.ques2no;
                break;
            case 21:
                etm.result = ite.ques1no == ite.ques2no;
                break;
            case 22:
                etm.result = ite.ques1no >= ite.ques2no;
                break;
            case 23:
                etm.result = ite.ques1no > ite.ques2no;
                break;
            case 24:
                etm.result = ite.ques1no <= ite.ques2no;
                break;
            case 25:
                etm.result = ite.ques1no < ite.ques2no;
                break;
            case 26:
                etm.result = ite.ques1no != ite.ques2no;
                break;
            case 27://无条件跳转
            case 40:
            case 41:
            case 42:
            case 43:
                break;
            case 28:
                etm.result = (ite.ques1no == ite.ques2no);
                break;
            case 29:
                etm.result = (ite.ques1no != ite.ques2no);
                break;
            case 30:
                etm.result = (ite.ques1no >= ite.ques2no);
                break;
            case 31:
                etm.result = (ite.ques1no <= ite.ques2no);
                break;
            case 32:
                etm.result = (ite.ques1no > ite.ques2no);
                break;
            case 33:
                etm.result = (ite.ques1no < ite.ques2no);
                break;
            case 34:
                etm.result = ite.ques1no == 0;
                break;
            case 35:
                etm.result = ite.ques1no != 0;
                break;
            case 36:
                etm.result = ite.ques1no <= 0;
                break;
            case 37:
                etm.result = ite.ques1no >= 0;
                break;
            case 38:
                etm.result = ite.ques1no > 0;
                break;
            case 39:
                etm.result = ite.ques1no < 0;
                break;
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
                etm.result = ite.ques1no;
                break;
            case 54:
                break;
            case 55:
                etm.result = ite.ques1no;
                etm._result = ite.ques2no;
                break;
        }
        ite.ansno = -1;
    }
};

class MA{
public:
    void run(){
        mtw.address = etm.address;
        mtw.operno = etm.operno;
        mtw.ansno = etm.ansno;
        //cout<<etm.address<<"&&&"<<etm.result<<endl;
        if(etm.operno >= 45 && etm.operno <= 50) {
            switch (etm.operno) {
                case 45:
                    mtw.result = data[etm.result];
                    break;
                case 46:
                    mtw.result += data[etm.result] << 8;
                    mtw.result += data[etm.result + 1];
                    break;
                case 47:
//                    cout<<etm.address<<"&&&"<<etm.result<<endl;
                    mtw.result = data[etm.result] << 24;
                    mtw.result += data[etm.result + 1] << 16;
                    mtw.result += data[etm.result + 2] << 8;
                    mtw.result += data[etm.result + 3];
                    break;
                case 48:
                    data[etm.result] = rgt[etm.ansno];
                    break;
                case 49:
                    data[etm.result] = rgt[etm.ansno] >> 8;
                    data[etm.result + 1] = rgt[etm.ansno];
                    break;
                case 50:
                    data[etm.result] = (rgt[etm.ansno]) >> 24;
                    data[etm.result + 1] = (rgt[etm.ansno]) >> 16;
                    data[etm.result + 2] = (rgt[etm.ansno]) >> 8;
                    data[etm.result + 3] = (rgt[etm.ansno]);
                    break;
            }
        }
        else if(etm.operno == 55) {
            //cout<<etm.address<<"&&&"<<etm.result<<endl;
            mtw.result = etm.result;
            if(etm.result == 1){
                cout<<etm._result;
            }
            else if(etm.result == 4){
                string tmp;
                int i = etm._result;
                while(data[i] != '\0'){
                    tmp += data[i];
                    ++i;
                }
                cout<<tmp;
            }
            else if(etm.result == 5) {
                //cout<<"请输入数字";
                cin>>mtw._result;
            }
            else if(etm.result == 9) {
                mtw._result = pile;
                pile += etm._result;
            }
            else if(etm.result == 8) {
                //cout<<"请输入字符串";
                string s;
                cin>>s;
                int i;
                for(i = 0; i < s.size(); ++i) {
                    data[i + etm._result] = s[i];
                }
            }

        }
        else {
            mtw.result = etm.result;
            mtw._result = etm._result;
        }
        etm.ansno = -1;
    }
};

class WB{
public:
    void run() {
        switch(mtw.operno) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 10:
            case 11:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 44:
            case 45:
            case 46:
            case 47:
                rgt[mtw.ansno] = mtw.result;
                break;
            case 8:
            case 9:
            case 12:
            case 13:
                lo = mtw.result;
                hi = mtw._result;
                break;
            case 27:
            case 40:
                pc = mtw.ansno;
                start = 1;
                hazard = 0;
                ite.ansno = etm.ansno = mtw.ansno = -1;
                break;
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
                if(mtw.result == 1) {
                    pc = mtw.ansno;
                    hazard = 0;
                    ite.ansno = etm.ansno = mtw.ansno = -1;
                    start = 1;
                }
                break;
            case 41:
                pc = rgt[mtw.ansno];
                start = 1;
                hazard = 0;
                ite.ansno = etm.ansno = mtw.ansno = -1;
                break;
            case 42:
                rgt[31] = mtw.address + 1;
                pc = mtw.ansno;
                start = 1;
                hazard = 0;
                ite.ansno = etm.ansno = mtw.ansno = -1;
                break;
            case 43:
                rgt[31] = mtw.address + 1;
                pc = rgt[mtw.ansno];
                start = 1;
                hazard = 0;
                ite.ansno = etm.ansno = mtw.ansno = -1;
                break;
            case 51:
                rgt[mtw.ansno] = mtw.result;
                break;
            case 52:
                rgt[mtw.ansno] = hi;
                break;
            case 53:
                rgt[mtw.ansno] = lo;
                break;
            case 54:
                pc = mtw.address + 1;
                start = 1;
                break;
            case 55:
                string s;
                if(mtw.result == 5) {
                    rgt[2] = mtw._result;
                }
                else if(mtw.result == 9) {
                    rgt[2] = mtw._result;
                }
                else if(mtw.result == 10 || mtw.result == 17) {
                    stop = 1;
                }
                break;
        }
        mtw.ansno = -1;
    }
};

void readData(string tmp){
    ifstream fin(tmp);
    string s;
    pc = 0;
    bool state;//类型：0(data),1(text)
    while(getline(fin,s)){
        while(s[0] == '\t' || s[0] == ' '){//删除首端空格
            s = s.substr(1, s.size() - 1);
        }
        if(s == "") continue;//删除空行
        if(s[0] == '.'){
            if(s[4] == 'a'){//.data
                state = 0;
            }
            else if(s[4] == 't'){//.text
                state = 1;
            }
            else if(s[4] == 'g'){//.align
                if((pc + 1) % 4 != 0){
                    pc = pc + 5 - (pc + 1) % 4;
                }
            }
            else if(s[4] == 'i'){//.ascii
                int i = 5;
                for(; s[i] != '"'; ++i) {}
                ++i;
                for(; s[i] != '"'; ++i) {
                    if(s[i] == '\\' && s[i + 1] == 'n'){
                        data[pc++] = '\n';
                        ++i;
                        continue;
                    }
                    else if(s[i] == '\\' && s[i + 1] == '"'){
                        s[i + 1] = 'p';
                        data[pc++] = '"';
                        ++i;
                        continue;
                    }
                    data[pc++] = s[i];
                }
                data[pc++] = '\0';
            }
            else if(s[4] == 'e'){//.byte
                int tmp = 0;
                for(int i = 6; i <= s.size(); ++i){
                    if(s[i] == ' ')continue;
                    else if(s[i] == ',' || i == s.size()){
                        data[pc++] = tmp;
                        tmp = 0;
                    }
                    else{
                        tmp = tmp * 10 + (s[i] - '0');
                    }
                }
            }
            else if(s[4] == 'f'){//.half
                int tmp = 0;
                for(int i = 6; i <= s.size(); ++i) {
                    if(s[i] == ' ') continue;
                    else if(s[i] == ',' || i == s.size()) {
                        data[pc++] = tmp >> 8;
                        data[pc++] = tmp;
                        tmp = 0;
                    }
                    else{
                        tmp = tmp * 10 + (s[i] - '0');
                    }
                }
            }
            else if(s[4] == 'd'){//.word
                int tmp = 0;
                for(int i = 6; i <= s.size(); ++i) {
                    if(s[i] == ' ') continue;
                    else if(s[i] == ',' || i == s.size()) {
                        data[pc++] = tmp >> 24;
                        data[pc++] = tmp >> 16;
                        data[pc++] = tmp >> 8;
                        data[pc++] = tmp;
                        tmp = 0;
                    }
                    else{
                        tmp = tmp * 10 + (s[i] - '0');
                    }
                }
            }
            else{//.space
                int tmp = 0;
                for(int i = 7; i < s.size(); ++i) {
                    tmp = tmp * 10 + (s[i] - '0');
                }
                pc += tmp;
            }
            continue;
        }
        else if(s[s.size() - 1] == ':'){//find label
            s.pop_back();
            if(state == 0){//dataLabel
                dataMap[s] = pc;
            }
            else{//textLabel
                textMap[s] = dda.size();
            }
        }
        else{//text
            dda.push_back(s);
        }
    }
}

void printData(){
    for(int i = 0; i < dda.size(); ++i){
        cout<<i<<" "<<dda[i]<<endl;
    }
}

IF iif;
ID iid;
EX eex;
MA mma;
WB wwb;

int main(int argc, char * argv[]) {
    readData(argv[1]);//读取文件
    pile = pc;
    //printData();
    pc = textMap["main"];
    rgt[29] = 2000000;
    ite.ansno = etm.ansno = mtw.ansno = -1;
    
    while(stop != 1){
        //cout<<rgt[8]<<"***"<<pc<<endl;
        if(start >= 5){
            if(start == 5) {
                ++start;
            }
            wwb.run();
        }
        if(start >= 4 && stop != 1){
            if(start == 4) {
                ++ start;
            }
            mma.run();
        }
        if(hazard == 1 && stop != 1) {
            wwb.run();
            start = 2;
            hazard = 0;
            //cout<<"###"<<endl;
        }
        if(start >= 3 && stop != 1){
            if(start == 3){
                ++start;
            }
            eex.run();
        }
        if(start >= 2 && stop != 1) {
            if(start == 2){
                ++start;
            }
            iid.run();
        }
        if(hazard == 0 && start >= 1 && stop != 1) {
            if(start == 1){
                ++start;
            }
            iif.run();
        }
    }
    
    return 0;
}

